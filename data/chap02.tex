
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

\chapter{风险管理指标}
在金融业的风险管理中，Value-at-risk（VaR）与Conditional value-at-risk（CVaR）是两个使用非常广泛的风险衡量指标。他们在投资、风险管理以及对金融机构的监管方面扮演着重要的角色。Basel Accord II将$ VaR_\alpha$的定义具体化为投资组合价值$L$的$\alpha$分位数，并且建议银行使用VaR进行日常风险管理。$CVaR_\alpha$定义为$VaR_\beta$的平均值，其中$0<\beta<\alpha$，在保险领域有着很长的应用历史。它提供了未来投资者可能遭受的大额损失的信息\cite{sun2010asymptotic}。

\section{VaR与CVaR的定义}

记损失函数为$Y$。令$F_Y$为它的分布函数，即$F_Y(u)=P\{Y\leq u\}$。令$F_Y^{-1}(v)$为其右连续逆函数，即$F_Y^{-1}(v)=\inf\{u:F_Y(u)>v\}$。

对固定的水平$\alpha$,定义$VaR_{\alpha}$为$\alpha$分位点，即
\begin{equation}
VaR_{\alpha}=F^{-1}(\alpha).
\end{equation}
$CVaR_{\alpha}$定义为一个优化问题的解
\begin{equation}
\label{chap2:1}
CVaR_{\alpha}(Y):=\inf \{a+\frac{1}{1-\alpha}E[Y-a]^+:a\in \mathbb{R}\},
\end{equation}
其中$[z]^+=\max(z,0)$。Uryasev与Rockafellar曾经证明了CVaR的另一种等价定义形式\cite{rockafellar2000optimization}：
CVaR等于在给定$Y\geq VaR_\alpha$的条件下，$Y$的条件期望，即：
$$CVaR_\alpha(Y)=E[Y|Y\geq VaR_\alpha(Y)].$$
事实上，目前我们经常使用条件期望形式作为$CVaR_\alpha$的定义。

\section{VaR与CVaR的性质}

接下来，我们介绍一些VaR与CVaR的性质，以及他们之间的关系。
以下性质保证了即使在$F$不可微的情况下，$VaR_\alpha$使得~\ref{chap2:1}取最小值。

\begin{proposition}
假设$F(b)\geq \alpha$以及$F(b-)\leq \alpha$。则对任意的a，都有
$$b+\frac{1}{1-\alpha}E[Y-b]^+\leq a+\frac{1}{1-\alpha}E[Y-a]^+.$$
\end{proposition}
由上述性质可知
$$VaR_\alpha(Y)=F^{-1}(\alpha)\in \arg \min\{a+\frac{1}{1-\alpha}E[Y-a]^+\}.$$
因此，CVaR的另一种等价形式为：
\begin{align*}
CVaR_{\alpha}(Y)&=E[Y|Y\geq F^{-1}(\alpha)]\\
                &=\frac{1}{1-\alpha}\int_\alpha^1 F^{-1}(v)dv\\
                &=\frac{1}{1-\alpha}\int_{F^{-1}(\alpha)}^\infty udF(u).
\end{align*}
Fishburn指出风险测度的性质可以用控制关系导出的偏好性结构来规范化表达\cite{fishburn1980stochastic}。
记$Y_1$与$Y_2$为两个随机变量。

\begin{definition}
阶为1的随机控制关系：
$$Y_1\prec_{SD(1)}Y_2,$$
当且仅当对任意的可积单调函数$\psi$，有
$$E[\psi(Y_1)]\leq E[\psi(Y_2)].$$
\end{definition}

\begin{definition}
阶为2的随机控制关系：
$$Y_1\prec_{SD(2)}Y_2,$$
当且仅当对任意的可积单调凹函数$\psi$，有
$$E[\psi(Y_1)]\leq E[\psi(Y_2)].$$
\end{definition}

\begin{definition}
阶为2的单调控制关系：
$$Y_1\prec_{MD(2)}Y_2,$$
当且仅当对任意的可积凹函数$\psi$，有
$$E[\psi(Y_1)]\leq E[\psi(Y_2)].$$
\end{definition}
有如下简单关系：
$$Y_1\prec_{SD(1)}Y_2 \Rightarrow Y_1\prec_{SD(2)}Y_2,$$
$$Y_1\prec_{SD(2)}Y_2 \Leftrightarrow Y_1\prec_{SD(1)}Y_2,Y_1\prec_{MD(2)}Y_2.$$
$CVaR_\alpha$有如下性质：

\begin{proposition}
平移不变性
$$CVaR_\alpha(Y+c)=CVaR_\alpha(Y)+c.$$
\end{proposition}

\begin{proposition}
正齐次性 \\
对任意$c>0$，有
$$CVaR_\alpha(cY)=cCVaR_\alpha(Y).$$
\end{proposition}

\begin{proposition}
如果$Y$的概率密度函数存在，则有
$$E(Y)=(1-\alpha)CVaR_\alpha(Y)-\alpha CVaR_{1-\alpha}(-Y).$$
\end{proposition}

\begin{proposition}
凸性\\
对随机变量$Y_1$与$Y_2$，以及$0<\lambda<1$，有
$$CVaR_\alpha(\lambda Y_1+(1-\lambda)Y_2)\leq \lambda CVaR_\alpha(Y_1)+(1-\lambda)CVaR_\alpha(Y_2).$$
\end{proposition}

\begin{proposition}
$CVaR_\alpha$在SD(2)意义下单调
$$Y_1 \prec_{SD(2)} Y_2 \Rightarrow CVaR_\alpha(Y_1)\leq CVaR_\alpha(Y_2).$$
\end{proposition}

\begin{proposition}
$CVaR_\alpha$在MD(2)意义下单调
$$Y_1 \prec_{MD(2)} Y_2 \Rightarrow CVaR_\alpha(Y_1)\leq CVaR_\alpha(Y_2).$$
\end{proposition}
Artzner,Delbaen,Eber以及Heath称一个风险测度是一致的，如果它满足平移不变性、凸性、正齐次性以及$\prec_{SD(1)}$意义下单调\cite{artzner1999coherent}。
不难发现，$CVaR_\alpha$是一致的。另一方面，$VaR_\alpha$不一致，因为它不满足凸性。

\begin{definition}
两个随机变量$Y_1$，$Y_2$定义在同一个概率空间$(\Omega,A,P)$上，称他们是共单调的，如果对任意$\omega,\omega '\in \Omega$有
$$[Y_1(\omega)-Y_2(\omega)][Y_1(\omega ')-Y_2(\omega ')]\geq 0,a.s.$$
\end{definition}
Shaun Wang给出另一个等价的定义为\cite{wang1997axiomatic}，$Y_1$与$Y_2$共单调，如果存在函数$f$，$g$单调增以及$U$为[0,1]上的均匀分布使得$Y_1=f(U)$，$Y_2=g(U)$。 \\
$VaR_\alpha$满足如下性质：
\begin{proposition}
平移不变性
$$VaR_\alpha(Y+c)=VaR_\alpha(Y)+c.$$
\end{proposition}

\begin{proposition}
正齐次性
$$VaR_\alpha(cY)=cVaR_\alpha(Y).$$
\end{proposition}

\begin{proposition}
$$VaR_\alpha(Y)=-VaR_{1-\alpha}(-Y).$$
\end{proposition}

\begin{proposition}
$$Y_1\prec SD(1) Y_2 \Rightarrow VaR_\alpha(Y_1)\leq VaR_\alpha(Y_2).$$
\end{proposition}

\begin{proposition}
共单调可加性\\
如果$Y_1$与$Y_2$共单调，则有
$$VaR_\alpha(Y_1+Y_2)=VaR_\alpha(Y_1)+VaR_\alpha(Y_2).$$
\end{proposition}

\section{VaR与CVaR的关系}
理论上，VaR与CVaR衡量了分布的不同性质。VaR是分位数，CVaR是尾部条件期望。记$[Y]^c=\min(Y,c)$。令$c=VaR_\alpha(Y)$，则有$CVaR_\alpha([Y]^c)=VaR_\alpha(Y).$

\begin{proposition}
$CVaR_\alpha\geq VaR_\alpha(Y).$
\end{proposition}

\begin{proposition}
$VaR_\alpha(Y)=\sup\{v:CVaR_\alpha([Y]^v)=v\}.$
\end{proposition}

\begin{proposition}
如果$Y$非负，则当$n\rightarrow \infty$时，有
$$\bigg[\frac{E(Y^n)-(1-\alpha)CVaR_\alpha(Y^n)}{\alpha}\bigg]^{1/n} \rightarrow VaR_\alpha(Y).$$
\end{proposition}