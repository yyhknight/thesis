
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

\chapter{VaR与CVaR的随机模拟}
风险管理人为了获得关于投资组合风险的更多信息，需要同时考虑VaR与CVaR。通常情况下，有三种典型的方法去估计它们：方差-协方差方法、历史数据模拟方法以及蒙特卡洛模拟方法。在这三种方法之中，蒙特卡洛模拟是使用频率最高的方法，因为它的使用方式更加普遍，并且可以应用到更广泛的风险模型之中。

\section{蒙特卡洛模拟}

\subsection{估计量的表示}
沿用之前的记号，记损失函数L的分布函数为F，我们有\\
$VaR_\alpha$为
$$v_\alpha=F^{-1}(\alpha).$$
$CVaR_\alpha$为
$$c_\alpha=\frac{1}{1-\alpha}\int_\alpha^1 v_\beta d\beta,$$
或
$$c_\alpha=\inf_{t\in \mathbb{R}}\{t+\frac{1}{1-\alpha}E[L-t]^+\}.$$
记$T$为上述随机系统优化问题的最优解集合，Trindade证明了$T=[v_\alpha,u_\alpha]$\cite{trindade2007financial}，其中$u_\alpha=\sup\{t:F(t)\leq \alpha\}$。当L在$v_\alpha$的某个领域里概率密度函数为正时，有$v_\alpha=u_\alpha$，此时上述的随机系统优化问题有唯一解
$$c_\alpha=v_\alpha+\frac{1}{1-\alpha}E[L-v_\alpha]^+,$$
并且
$$c_\alpha=E[L|L\geq v_\alpha].$$
上式等号右边部分也被称为期望损失或者尾部条件期望。

设$L_1,L_2,...,L_n$为损失函数$L$的n个独立同分布的观察。则$VaR_\alpha$可以用以下式子估计：
$$\hat{v}_\alpha^n=L_{\lceil n\alpha\rceil:n},$$
其中$L_{i:n}$为$n$个观察中的第$i$个顺序统计量。\\
Trindade提出$CVaR_\alpha$的一个估计\cite{trindade2007financial}：
$$\hat{c}_\alpha^n=\inf_{t\in \mathbb{R}}\{t+\frac{1}{n(1-\alpha)}\sum_{i=1}^n[L_i-t]^+\}.$$

但使用上述表达式需要解一个随机优化系统。当L在$v_\alpha$的某个领域里概率密度函数为正时，我们有更简单的估计方式。记$L_1,L_2,...,L_n$的经验分布函数为$F_n(y)=\frac{1}{n}\sum_{i=1}^n 1_{\{L_i\leq y\}}$
，其中$1_{\{\cdot\}}$为示性函数。则有
$$\hat{c}_\alpha^n=\inf_{t\in \mathbb{R}}\{t+\frac{1}{1-\alpha}E[\tilde{L}-t]^+\},$$
其中$\tilde{L}$的分布函数为$F_n$。由于$\hat{v}_\alpha^n=F_n^{-1}(\alpha)$，我们得到
$$\hat{c}_\alpha^n=\hat{v}_\alpha^n+\frac{1}{n(1-\alpha)}\sum_{i=1}^n[L_i-\hat{v}_\alpha^n]^+.$$
因此，我们可以直接使用上述方式进行估计，从而避免了随机优化系统的求解。

\subsection{估计量的渐进性}
估计量$\hat{v}_\alpha^n$与$\hat{c}_\alpha^n$相合性与渐近性已经在许多文章里被广泛地研究过了。作为统一的观点，这里使用Bahadur的表述方式来列举$\hat{v}_\alpha^n$与$\hat{c}_\alpha^n$的渐近性质\cite{bahadur1966note}。

假设存在$\epsilon>0$使得$L$在区间$(v_\alpha-\epsilon,v_\alpha+\epsilon)$中任一点$x$，均有正且连续可微的概率密度函数$f(x)$。
此时我们有$F(v_\alpha)=\alpha$，$c_\alpha=E[L|L\geq v_\alpha]$以及如下的性质\cite{bahadur1966note}。

\begin{proposition}
$$\hat{v}_\alpha^n=v_\alpha-\frac{1}{f(v_\alpha)}\bigg((1-\alpha)-\frac{1}{n}\sum_{i=1}^n 1_{\{L_i\geq v_\alpha\}}\bigg)+A_n,$$
$$\hat{c}_\alpha^n=c_\alpha+\bigg(\frac{1}{n}\sum_{i=1}^n[v_\alpha+\frac{1}{1-\alpha}(L_i-v_\alpha)^+]-c_\alpha\bigg)+B_n.$$
\end{proposition}
其中$A_n=O_{a.s.}(n^{-3/4}(\log n)^{3/4})$，$B_n=O_{a.s.}(n^{-1}\log n)$。$X_n=O_{a.s.}(g(n))$表示$X_n/g(n)$几乎处处被一个常数控制。

相合性与渐近正态性可以直接由上述性质推导得出。即，当$n\rightarrow \infty$时，依概率1有，$\hat{v}_\alpha^n\rightarrow v_\alpha$，$\hat{c}_\alpha^n \rightarrow c_\alpha$。并且，如果有$E(L^2)<\infty$，则当$n \rightarrow \infty$时，有

$$\sqrt{n}(\hat{v}_\alpha^n-v_\alpha)\xrightarrow{d} \frac{\sqrt{\alpha(1-\alpha)}}{f(v_\alpha)}N(0,1),$$
$$\sqrt{n}(\hat{c}_\alpha^n-c_\alpha)\xrightarrow{d} \sigma_\infty N(0,1),$$
其中$\xrightarrow{d}$表示依分布收敛，$N(0,1)$表示标准正态分布随机变量，
$$\sigma_\infty^2=\lim_{n\rightarrow \infty}n Var(\hat{c}_\alpha^n)=\frac{1}{(1-\alpha)^2} Var([L-v_\alpha]^+).$$

\section{重要性抽样}
记$L$为实值随机变量，分布函数为$F(.)$，记v，c为$L$的$VaR_\alpha$与$CVaR_\alpha$。则有，
$$v=F^{-1}(\alpha)=\inf\{x:F(x)\geq \alpha\},$$
$$c=v+\frac{1}{1-\alpha}E[L-v]^+,$$
其中$x^+=\max\{x,0\}$。

如果$L$在$v$的一个领域内有正的概率密度函数，则$v$为$L$的$\alpha$分位数，$c=E[L|L\geq v]$。由于我们感兴趣的地方在于$L$的左尾分布情况。因此，$\alpha$
通常非常接近于0。

通常情况下对于$v,c$的蒙特卡洛模拟方法为抽取独立同分布的$L$的样本，记为$L_1,...,L_n$，估计$v,c$为
$$\hat{v}_n=\tilde{F}_n^{-1}=\inf\{x:\tilde{F}_n(x)>\alpha\},$$
$$\hat{c}_n=\hat{v}_n+\frac{1}{n(1-\alpha)}\sum_{i=1}^n(L_i-\hat{v}_n)^+.$$
其中$\tilde{F}_n(x)=\frac{1}{n}\sum_{i=1}^n 1_{\{L_i\leq x\}}$为$L$的经验分布函数，$1_{\{\cdot\}}$为示性函数。注意到$\tilde{F}_n(x)$是$F(x)$的无偏、相合估计。Serfling和Trindade证明了当$n\rightarrow \infty$时，$\hat{v}_n$与$\hat{c}_n$分别为$v,c$的相合估计\cite{trindade2007financial}。

\subsection{估计量表示}
接下来引入重要性抽样方法。设我们选择一个重要性抽样测度，它的分布函数为$G$。$G$的概率密度函数对$F$的概率密度函数绝对连续，即$G(dx)=0 \Rightarrow F(dx)=0,\forall x\in \mathbb{R}$。记$\mathcal{L}(x)=\frac{F(dx)}{G(dx)}$，$\mathcal{L}$称为似然比函数。对任意的$x\in \mathbb{R}$，我们可以得到$F(x)$ 的估计为：
$$F_n(x)=\frac{1}{n}\sum_{i=1}^n1_{\{L_i\leq x\}}\mathcal{L}(L_i).$$
不难看出$F_n(x)$也是$F(x)$的一个无偏、相合估计。记$v_n,c_n$为$v,c$的重要性抽样估计。类似的，我们有
$$v_n=F_n^{-1}(\alpha)=\inf\{x:F_n(x)\geq \alpha\},$$
$$c_n=v_n+\frac{1}{n(1-\alpha)}\sum_{i=1}^n(L_i-v_n)^+\mathcal{L}(L_i).$$
为了分析$v_n,c_n$的渐近性质，我们需要以下两个假设。

\begin{assumption}
\label{chap3:assumption1}
存在$\epsilon>0$使得$L$在$x\in(v-\epsilon,v+\epsilon)$上有正并且连续可微的概率密度函数$f(x)$。
\end{assumption}
假设~\ref{chap3:assumption1}要求$L$在$v$的一个领域内有正且可微的概率密度函数。这保证了$F(v)=\alpha$以及$c=E[L|L\geq v]$。

\begin{assumption}
\label{chap3:assumption2}
存在$\epsilon>0$以及$C>0$，使得对任意$x\in (v-\epsilon,v+\epsilon)$有$\mathcal{L}(x)<C$以及存在$p>2$，使得$E_G[1_{\{L>v-\epsilon\}}\mathcal{L}^p(L)]<\infty$，其中$E_G$表示在重要性抽样测度意义下计算期望。
\end{assumption}
假设~\ref{chap3:assumption2}要求似然比函数在$v$的一个领域内有界并且在$L$的右尾上$p>2$阶矩有限。一个有效的重要性抽样分布通常会满足在$x>v-\epsilon$ 上有$g(x)>f(x)$
。这样我们可以将更多的抽样聚集在$\{L>v-\epsilon\}$上，这对于估计$v,c$是相当有利的。此外，由假设3.2我们可以得到对任意$\epsilon'<\epsilon$,
$E_G[1_{\{L>v-\epsilon'\}}\mathcal{L}^2(L)]<\infty$。因此，对任意$\epsilon'<\epsilon$，$Var[1_{\{L>v-\epsilon'\}}\mathcal{L}(L)]<\infty$。


\subsection{估计量的渐进性}
一个复杂的估计量通常可以表示为若干渐近性质简单的几项之和。这一表示方式被称为估计量的渐近表示。基于这一方法，许多估计量的渐近性质，例如相合性与渐近正态性都可以容易地分析。

回忆之前的内容，对于VaR的估计量$\hat{v_n}$来说，有一个非常著名的渐近表示\cite{bahadur1966note}（Bahadur representation of the quantile estimator）。在假设3.1 下，有
$$\hat{v}_n=v-\frac{1}{f(v)}\bigg((1-\alpha)-\frac{1}{n}\sum_{i=1}^n1_{\{L_i\geq v\}}\bigg)+R_n,$$
其中$R_n=O_{a.s.}(n^{-3/4}(\log n)^{3/4})$。$Y_n=O_{a.s.}(g(n))$表示$Y_n/g(n)$几乎处处被一个常数控制。有了这一渐近表示，$\hat{v}_n$的许多渐近性质可以很容易地得到。由强大数定理，当$n\rightarrow \infty$时，依概率1，有$\frac{1}{n}\sum_{i=1}^n 1_{\{L_i\leq v\}}\rightarrow F(v)$。此外，因为$F(v)=\alpha$，所以当$n\rightarrow \infty$时，依概率1，有$\hat{v}_n\rightarrow v$。因此，$\hat{v}_n$是$v$的强相合估计量。类似的，由中心极限定理，当$n\rightarrow \infty$时，有
$$\sqrt{n}\bigg((1-\alpha)-\frac{1}{n}\sum_{i=1}^n1_{\{L_i\geq v\}}\bigg)\xrightarrow{d}\sqrt{\alpha(1-\alpha)}N(0,1),$$
其中$\xrightarrow{d}$表示依分布收敛。$N(0,1)$表示标准正态随机变量。因此有，当$n \rightarrow \infty$时，有
$$\sqrt{n}(\hat{v}_n-v)\xrightarrow{d} \frac{\sqrt{\alpha(1-\alpha)}}{f(v)}N(0,1).$$
因此，$\hat{v}_n$有渐近正态性。

接下来，我们将给出重要性抽样估计量$v_n,c_n$的渐近表示，以此可以得到$v_n,c_n$的相合性与渐近正态性。\\
$v_n$的渐近表示：
由泰勒展开，$F(v_n)-F(v)=f(v)(v_n-v)-A_{1,n}$，其中$A_{1,n}$是剩余项。因此，我们有
$$v_n=v+\frac{F(v_n)-F(v)}{f(v)}+\frac{1}{f(v)}A_{1,n}.$$
记$A_{2,n}=F(v_n)+F_n(v)-F_n(v_n)-F(v)$以及$A_{3,n}=F_n(v_n)-F(v)$，\\
我们容易得到
$$F(v_n)-F(v)=F(v)-F_n(v)+A_{2,n}+A_{3,n}.$$
因此，我们有
$$v_n=v+\frac{F(v)-F_n(v)}{f(v)}+\frac{A_{1,n}+A_{2,n}+A_{3,n}}{f(v)}.$$
下面的引理给出了$A_{1,n},A_{2,n}$以及$A_{3,n}$的阶。

\begin{lemma}
对固定的$\alpha\in(0,1)$，假设1与假设2满足。则对任意$\delta>0$，有$A_{1,n}=O_{a.s.}(n^{-1+2/p+\delta})$，$A_{2,n}=O_{a.s.}(n^{-3/4+1/(2p)+\delta})$，$A_{3,n}=O_{a.s.}(n^{-1})$。以及$A_{1,n}=o_p(n^{-1/2})$，
$A_{2,n}=o_p(n^{-1/2})$，$A_{3,n}=o_p(n^{-1/2})$。
\end{lemma}
此外，如果对于任意$x\in(-\infty,v+\epsilon)$，有$\mathcal{L}(x)<C$，则有$A_{1,n}=O_{a.s.}(n^{-1}\log n)$，$A_{2,n}=O_{a.s.}(n^{-3/4}(\log n))^{3/4}$ 以及$A_{3,n}=O_{a.s.}(n^{-1})$。
其中$Y_n=o_p(g(n))$表示当$n\rightarrow \infty$时，依概率有$Y_n/g(n)\rightarrow 0$。

记$\mathcal{L}(L_i)$为$\mathcal{L}_i$,$i=1,2,...,n$。由引理3.1我们可以证明如下定理。
\begin{theorem}
对固定的$\alpha \in(0,1)$，假设1与假设2成立。则，
$$v_n=v-\frac{1}{f(v)}\bigg((1-\alpha)-\frac{1}{n}\sum_{i=1}^n 1_{\{L_i\geq v\}}\mathcal{L}\bigg)+A_n,$$
\end{theorem}
其中$A_n=o_p(n^{-1/2})$以及对任意$\delta>0$，$A_n=O_{a,s.}(t(n,\delta))$。$t(n,\delta)=\max(n^{-1+2/p+\delta},n^{-3/4+1/(2p)+\delta})$。
此外，如果对任意$x\in (-\infty,v+\epsilon)$有$\mathcal{L}(x)<C$，则有$A_n=O_{a.s.}(n^{-3/4}(\log n)^{3/4})$。
由于$\hat{v}_n$可以看成$\mathcal{L}(x)\equiv1$时，$v_n$的特殊情况，Bahadur表示也可以看作定理1的特殊情况。

记$Var_G$为在重要性抽样测度下的方差。由定理3.1，我们可以证明如下推论，即$v_n$的强相合性与渐近正态性。

\begin{corollary}
对于固定的$\alpha\in(0,1)$，假设1与假设2成立。则，当$n\rightarrow \infty$时，依概率1，有$v_n\rightarrow v$，且
$$\sqrt{n}(v_n-v)\xrightarrow {d}\frac{\sqrt{Var_G[1_{\{L\geq v\}}\mathcal{L}](L)}}{f(v)}N(0,1).$$
\end{corollary}

\begin{remark}
Glynn曾经在假设3.1成立以及$E_G[\mathcal{L}^3(L)]<\infty$成立的情况下，证明了推论3.1中的结论\cite{glynn1996importance}。
\end{remark}
注意到
\begin{align*}
Var_G[1_{\{L\geq v\}}\mathcal{L}(L)]&=E_G[1_{\{L\geq v\}}\mathcal{L}^2(L)]-E_G^2[1_{\{L\geq v\}}\mathcal{L}(L)]\\
                                    &=E[1_{\{L\geq v\}}\mathcal{L}(L)]-\alpha^2.
\end{align*}
如果对任意$x\geq v$,有$\mathcal{L}(x)<1$，则由等式(9)可以得到$Var_G[1_{\{L\geq v\}}\mathcal{L}(L)]<\alpha(1-\alpha)$。
通过对比可以发现重要性抽样估计量$v_n$的渐近方差比一般估计量$\hat{v}_n$的更小。因此，如果重要性抽样测度选取恰当，可以有效地提高VaR估计的效率。\\
考虑$c_n$的渐近表示。注意到
\begin{align*}
c_n=&v_n+\frac{1}{n(1-\alpha)}\sum_{i=1}^n(L_i-v_n)^+\mathcal{L}_i\\
=&v+\frac{1}{n(1-\alpha)}\sum_{i=1}^n(L_i-v)^+\mathcal{L}_i+(v_n-v)\\
&-\frac{1}{n(1-\alpha)}
\sum_{i=1}^n[(L_i-v_n)^+-(L_i-v)^+]\mathcal{L}_i.
\end{align*}
进一步，注意到
\begin{align*}
&\frac{1}{n(1-\alpha)}\sum_{i=1}^n[(L_i-v_n)^+-(L_i-v)^+]\mathcal{L}_i\\
=&\frac{1}{n(1-\alpha)}\sum_{i=1}^n[(L_i-v_n)1_{\{L_i\geq v_n\}}-(L_i-v)1_{\{L_i\geq v\}}]\mathcal{L}_i\\
=&\frac{1}{n(1-\alpha)}\sum_{i=1}^n[(v_n-v)1_{\{L_i\geq v_n\}}]\mathcal{L}_i+\frac{1}{n(1-\alpha)}\sum_{i=1}^n(L_i-v)[1_{\{L_i\geq v_n\}}-1_{\{L_i\geq v\}}]\mathcal{L}_i.
\end{align*}
则有，
\begin{align*}
c_n=&v+\frac{1}{n(1-\alpha)}\sum_{i=1}^n(L_i-v)^+\mathcal{L}_i+\frac{1}{1-\alpha}(v_n-v)(\alpha-F_n(v_n))\\
&-\frac{1}{n(1-\alpha)}\sum_{i=1}^n(L_i-v)[1_{\{L_i\geq v_n\}}-1_{\{L_i\geq v\}}]\mathcal{L}_i.
\end{align*}
由于
$$\mid\frac{1}{n(1-\alpha)}\sum_{i=1}^n(L_i-v)[1_{\{L_i\geq v_n\}}-1_{\{L_i\geq v\}}]\mathcal{L}_i\mid\leq \frac{1}{1-\alpha}|v_n-v|\cdot|F_n(v_n)-F_n(v)|.$$
我们有
$$c_n=v+\frac{1}{n(1-\alpha)}\sum_{i=1}^n(L_i-v)^+\mathcal{L}_i+B_n,$$
其中
$$|B_n|\leq \frac{1}{\alpha}|v_n-v|\bigg(2|F_n(v_n)-F(v)|+|F_n(v)-F(v)|\bigg).$$
以下引理表明了$B_n$的阶。

\begin{lemma}
对固定的$\alpha \in (0,1)$,假设3.1与假设3.2均成立。则，$B_n=O_{a.s.}(n^{-1+2/p+\delta})$以及$B_n=o_p(n^{-1/2})$。进一步，如果对任意$x\in (-\infty,v+\epsilon)$，有$\mathcal{L}(x)<C$，则$B_n=O_{a.s.}(n^{-1}\log n)$。
\end{lemma}
因此，我们有如下定理，即$c_n$的渐近表示。

\begin{theorem}
对固定的$\alpha\in (0,1)$，假设3.1与假设3.2均成立。则
$$c_n=c+\bigg(\frac{1}{n}\sum_{i=1}^n[v+\frac{1}{1-\alpha}(L_i-v)^+\mathcal{L}_i]-c\bigg)+B_n,$$
\end{theorem}
其中对任意$\delta>0$，有$B_n=O_{a.s.}(n^{-1+2/p+\delta})$以及$B_n=o_p(n^{-1/2})$。进一步，如果对任意$x\in (-\infty,v+\epsilon)$，有$\mathcal{L}(x)<C$,则$B_n=O_{a.s.}(n^{-1}\log n)$。

注意到$E_G[v+\frac{1}{1-\alpha}(L-v)^+\mathcal{L}(L)]=c$。则由强大数定律与中心极限定理，可以直接证明如下推论，即$c_n$的强相合性与渐近正态性。

\begin{corollary}
对固定的$\alpha \in (0,1)$，假设3.1与假设3.2成立。以及$E_G[(L-v)^2\mathcal{L}(L)^21_{\{L\geq v\}}]<\infty$。则，当$n \rightarrow \infty$ 时，依概率1，有$c_n\rightarrow c$以及
$$\sqrt{n}(c_n-c)\xrightarrow{d}\frac{\sqrt{Var_G[(L-v)^+\mathcal{L}(L)]}}{1-\alpha}N(0,1).$$
\end{corollary}
进一步，如果取$\mathcal{L}(x)\equiv 1$。则定理3.2与推论3.2可以应用到$c$的一般蒙特卡洛模拟估计量$\hat{c}_n$上。此时有如下推论。

\begin{corollary}
对固定的$\alpha \in (0,1)$，假设3.1成立。则有
$$\hat{c}_n=c+\bigg(\frac{1}{n}\sum_{i=1}^n[v+\frac{1}{1-\alpha}(L_i-v)^+]-c\bigg)+C_n,$$
\end{corollary}
其中$C_n=O_{a.s.}(n^{-1}\log n)$,以及当$n\rightarrow \infty$时，依概率1，有$\hat{c}_n\rightarrow c$。
进一步,如果$E[(L-v)^21_{\{L\geq v\}}]<\infty$，则
$$\sqrt{n}(\hat{c}_n-c)\xrightarrow{d}\frac{\sqrt{Var[(L-v)^+]}}{1-\alpha}N(0,1).$$

\begin{remark}
如果对任意$x\geq v$，重要性抽样测度满足$\mathcal{L}(x)<1$。容易证明$Var_G[(L-v)^+\mathcal{L}(L)]<Var[(L-v)^+]$。
因此由推论3.2与推论3.3可知，重要性抽样估计量$c_n$的渐近方差比一般估计量$\hat{c}_n$的渐近方差小。注意到在同样的条件下，重要性抽样估计量$v_n$的渐近方差也比一般估计量$\hat{v}_n$的渐近方差小。因此，如果重要性抽样测度选择恰当，可以同时提高VaR与CVaR估计量的效率。
\end{remark}

\section{拟蒙特卡洛模拟}
拟蒙特卡洛模拟与蒙特卡洛模拟有着很大的区别。它不需要进行随机模拟，其主要思想是将蒙特卡洛模拟中随机抽样的数字用确定的数字来替代。这些确定的数字必须非常巧妙的构造以达到更加均匀的分布。拟蒙特卡洛模拟在理论上有着更加快速的收敛速度，它将蒙特卡洛模拟的收敛速度$O(1/\sqrt{n})$提高到$O(1/n)$附近。在某些合适的条件下，对任意的$\epsilon<0$，收敛速度可以提升到$O(1/n^{1-\epsilon})$\cite{glasserman2003monte}。相比于蒙特卡洛模拟，拟蒙特卡洛模拟有许多优越性。由于本文研究的VaR与CVaR的估计需要我们在概率分布的尾部取得更多的均匀抽样。因此，结合重要性抽样，我们将尝试采用拟蒙特卡洛模拟方法，期望获得更大的数值模拟效率提升。

