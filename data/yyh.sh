#!/bin/bash

iconv -f GBK -t UTF-8 chap01.tex >> uchap01.tex
iconv -f GBK -t UTF-8 chap02.tex >> uchap02.tex
iconv -f GBK -t UTF-8 chap03.tex >> uchap03.tex
iconv -f GBK -t UTF-8 chap04.tex >> uchap04.tex
iconv -f GBK -t UTF-8 chap05.tex >> uchap05.tex
iconv -f GBK -t UTF-8 chap06.tex >> uchap06.tex
iconv -f GBK -t UTF-8 cover.tex >> ucover.tex
iconv -f GBK -t UTF-8 resume.tex >> uresume.tex
iconv -f GBK -t UTF-8 ack.tex >> uack.tex